export interface Book {
  id?: string;
  variation?: string;
  cover: string;
  title: string;
  author: string;
  favorite: boolean;
  about: string;
}
