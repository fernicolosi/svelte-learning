# README

Svelte tutorial from Pluralsight website with modifications:

- Add Typescript support
- Add SASS support

### How do I get set up?

- Run `npm install`
- Run `npm run dev`
- Navigate to `http://localhost:5000/`
